@extends('layout.master')
@section('judul')
<h3>Buat Account Baru</h3>
@endsection
<h5>Sign Up Form</h5>
@section('content')
<form action="/submit" method="post">
    @csrf
    <label>First Name  :</label>
    <input type="text" name="firstname" required><br>
    <label>Last Name   :</label>
    <input type="text" name="lastname" required><br>
    <label>Gender</label><br>
    <input type="radio" name="Gender" value="1">Male <br>
    <input type="radio" name="Gender" value="2">Female <br>
    <input type="radio" name="Gender" value="2">Other <br>
    <label>Nationality </label> <br>
    <select name="nationality"> <br>
        <option value="1">Indonesia</option>
        <option value="2">Rusia</option>
        <option value="3">Ukraina</option>
    </select> <br>
    <label >Language Spoken</label> <br>
    <input type="checkbox" name="Language"> Bahasa Indonesia <br>
    <input type="checkbox" name="Language"> English <br>
    <input type="checkbox" name="Language"> Arab <br>
    <input type="checkbox" name="Language"> Other <br>
    <label>Bio</label><br>
    <textarea name="bio" cols="40" rows="5" required></textarea> <br>
    <input type="submit" value="Sign Up">
</form>
      
  @endsection      
       
